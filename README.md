# Compilatoare si Translatoare

## Intro

This is a project I had to write for *"Compilatoare si Translatoare"* which in English it translates to the obvious *"Compilers and Interpreters"* which has been assigned by Professor Dragulici.

Anyhow, this project is just a lexer, it doesn't contain a parser and it's only made for the C languge (syntax) and therefore it cannot be categorized as a lexical analyzer.

## Lexer Graph

I wrote a graph of the lexer in the dot language and then rendered it as png. 

![](visuals/bordered_graph.jpg)

Every cell that's colored in green is a final state. Also the graph is separated in a couple of groups:
1. Group 1 - Indentifiers
1. Group 2 - Whitespace
1. Group 3 - Literal Float
1. Group 4 - Literal Integer
1. Group 5 - Literal Character
1. Group 6 - Comments
1. Group 7 - Literal String
1. Group 8 - Operators
1. Group 9 - Directives

## Installing It

To install it you can run
```bash
pip install git+https://gitlab.com/RobertLucian/compilatoare-si-translatoare
```

## Development

To develop it, clone the repo and run 
```bash
virtualenv -p python3 .env # make sure you've got python 3.6+ installed

.env\Scripts\activate # for Windows-based systems
source .env/bin/activate # for Linux-based systems

pip install --editable .
```

## Running It

Upon installing the package, you'll get a CLI tool that does the lexing for a given C file. The executable is called `clexer`.
And this is the description of the CLI tool.
```bash
Usage: clexer [OPTIONS] path_to_file

Options:
  --show         Send the output to STDOUT.
  --output TEXT  File to write the output to.
  --help         Show this message and exit.
```

## Test Files

There are a couple of test files found in `tests` directory. Use those to test them against this lexer.
