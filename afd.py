import numpy as np

class AFD(object):

    def __init__(self, no_states, final_states, transition_pairs, transition_values):
        '''
        no_states - number of states
        final_states - it's a vector with the final states
        transition_pairs - it's a vector of tuples showing the direction from one node to another
        transition_values - it's a vector of other vectors that contain the accepted characters for each transition pair
        '''

        self.transition_matrix = np.full((no_states, 256), fill_value=-1)

        self.no_states = no_states
        self.final_states = final_states
        
        for idx, pair in enumerate(transition_pairs):
            self.__set_transition_batch(pair[0], transition_values[idx], pair[1])

    def __set_transition_batch(self, initial_state, chars, trans_state):
        for char in chars:
            self.transition_matrix[initial_state, char] = trans_state

    def get_transition(self, state, char):
        return self.transition_matrix[state, char]
    
    def is_final_state(self, state):
        return state in self.final_states