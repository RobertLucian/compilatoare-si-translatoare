from lexical_analyzer import LexicalAnalyzerC
from lexical_config import no_states, final_states, transitions, transitions_values
import click

@click.command()
@click.argument('file_name', metavar='path_to_file')
@click.option('--show', is_flag=True, help='Send the output to STDOUT.')
@click.option('--output', default='', help='File to write the output to.')
def main(file_name, show, output):
    with open(file_name) as f:
        
        input_data = f.readlines()
        line_numbers = list(map(lambda x: len(x), input_data))
        input_data = ''.join(input_data)

        if len(output) > 0:
            write_file = True
            outf = open(output, 'w')
        else:
            write_file = False

        lex = LexicalAnalyzerC(no_states, final_states, transitions, transitions_values, input_data)

        (token_type, token_id, token, pos) = lex.get_token()
        while token_id != LexicalAnalyzerC.TOKENS.CHARACTER_ERROR and \
            token_id != LexicalAnalyzerC.TOKENS.CHARACTER_EOF:

            string = f"{token_type}, {token}"
            if show:
                click.echo(string)
            if write_file:
                outf.write(string + '\n')

            (token_type, token_id, token, pos) = lex.get_token()

        if token_id == LexicalAnalyzerC.TOKENS.CHARACTER_ERROR:
            
            line_counter = 0
            chars_counter = 0
            line_pos = 1
            for line, no_chars in enumerate(line_numbers):
                line_counter += 1
                chars_counter += no_chars
                if pos < chars_counter:
                    line_pos = line
                    break

            string = f"{token_type} on line {line_pos}"
            if show:
                click.echo(string)
            if write_file:
                outf.write(string + '\n')
        
        if write_file:
            outf.close()
        
if __name__ == "__main__":
    try:
        main()
    except SystemExit:
        pass