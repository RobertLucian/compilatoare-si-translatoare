no_states = 57

final_states = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 14, 16, 18, 19, 20, 21, 22, 23,
    27, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 49, 56
]

transitions = [
    [0, 1],  # identifier
    [1, 1],  # identifier
    [0, 6],  # whitespace
    [6, 6],  # whitespace
    [0, 2],  # literal int 0 
    [0, 3],  # literal int -
    [3, 2],  # literal int -0
    [0, 4],  # literal int 1-9
    [3, 4],  # literal int -1-9
    [4, 4],  # literal int (-)-9_0-9
    [33, 2],  # literal int +0
    [33, 4],  # literal int +1-9
    [2, 7],  # literal int octal
    [7, 7],  # literal int octal
    [2, 13],  # literal int hexa
    [13, 14],  # literal int hexa
    [14, 14],  # literal int hexa
    [2, 15],  # literal int bin
    [15, 16],  # literal int bin
    [16, 16],  # literal int bin
    [0, 8],  # line comment - / op
    [8, 9],  # transition towards line comment
    [9, 9],  # line comment
    [8, 10],  # block comment / -> *
    [10, 10],  # block comment * -> notstar
    [10, 11],  # block comment * -> *
    [11, 10],  # block comment * -> no slash or star
    [11, 11],  # block comment * -> *
    [11, 12],  # block comment * -> /
    [0, 5],  # separator
    [0, 22],  # separator
    [2, 21],  # literal float 0 -> 0
    [21, 21],  # literal float _. -> _.0-9
    [21, 17],  # literal float e,E
    [21, 20],  # literal float sufix f,F,d,D
    [17, 18],  # literal float _. -> _.0-9
    [17, 23],  # literal float _._e(+|-)
    [18, 18],  # literal float _. -> _.0-9
    [18, 20],  # literal float sufix f,F,d,D
    [23, 23],  # literal float _. -> _.0-9
    [19, 20],  # literal float sufix f,F,d,D
    [4, 21],  # literal float _ -> _.
    [4, 20], 
    [4, 17],  # literal float e,E
    [7, 21],  # literal float _ -> _.
    [7, 20], 
    [7, 17],  # literal float e,E
    [22, 21],  # literal float _. -> _.0-9
    [0, 24],  # literal character '
    [24, 25],  # literal character \
    [24, 26],  # literal character ' -> char. graphic
    [25, 26],  # literal character '\t' '\\' '\n' '\''
    [26, 27],  # literal character'
    [0, 28],  # literal character "
    [28, 28],  # literal character ' -> char. graphic
    [28, 29],  # literal string \
    [28, 31],  # literal string "
    [29, 30],  # literal string '\t' '\\' '\n' '\''
    [30, 28],  # literal string (graphic chars)
    [30, 29],  # literal string \n -> \
    [30, 31],  # literal string "
    [0, 32],  # operator
    [0, 33],  # operator +
    [33, 32],  # operator ++, +=
    [3, 32],  # operator --, -=
    [8, 32],  # operator /=
    [0, 35],  # operator |
    [35, 32],  # operator ||, |=
    [0, 36],  # operator &
    [36, 32],  # operator &&, &=
    [0, 37],  # operator * ! ^ %
    [37, 32],  # operator *= != ^= %=
    [0, 38],  # operator =
    [38, 32],  # operator ==
    [0, 39],  # operator <
    [39, 32],  # operator <=
    [39, 41],  # operator <<
    [41, 32],  # operator <<=
    [0, 40],  # operator >
    [40, 32],  # operator >=
    [40, 42],  # operator >>
    [42, 32],  # operator >>>, >>=

    [0, 43],  # for # directive
    [43, 44],  # for d
    [44, 45],  # for e
    [45, 46],  # for f
    [46, 47],  # for i
    [47, 48],  # for n
    [48, 49],  # for e
    [43, 50],  # for i
    [50, 51],  # for n
    [51, 52],  # for c
    [52, 53],  # for l
    [53, 54],  # for u
    [54, 55],  # for d
    [55, 56]  # for e
    ]
transitions_values = [
    list(range(65, 91)) + list(range(97, 123)) + [95, 36],
    list(range(65, 91)) + list(range(97, 123)) + [95, 36] + list(range(48, 58)),
    list(range(9, 14)) + [32],
    list(range(9, 14)) + [32],
    [48],
    [45],
    [48],
    list(range(49, 58)),
    list(range(49, 58)),
    list(range(48, 58)),
    [48],
    list(range(49, 58)),
    list(range(48, 58)),
    list(range(48, 58)),
    [88, 120],
    list(range(48, 58)) + list(range(97, 103)) + list(range(65, 71)),
    list(range(48, 58)) + list(range(97, 103)) + list(range(65, 71)),
    [66, 98],
    [48, 49],
    [48, 49],
    [47],
    [47],
    [x for x in range(256) if x != 10 and x != 13],
    [42],
    [x for x in range(256) if x != 42],
    [42],
    [x for x in range(256) if x != 42 and x != 47],
    [42],
    [47],
    [59, 44, 40, 41, 91, 93, 123, 125],
    [46],
    [46],
    list(range(48, 58)),
    [69, 101],
    [70, 102, 68, 100],
    list(range(48, 58)),
    [43, 45],
    list(range(48, 58)),
    [70, 102, 68, 100],
    list(range(48, 58)),
    [70, 102, 68, 100],
    [46],
    [70, 102, 68, 100],
    [69, 101],
    [46],
    [70, 102, 68, 100],
    [69, 101],
    list(range(48, 58)),
    [39],
    [92],
    [x for x in range(32, 127) if x != 39 and x != 92],
    [39, 110, 118, 98, 114, 102, 92],
    [39],
    [34],
    [x for x in range(32, 127) if x != 34 and x != 92],
    [92],
    [34],
    [34, 110, 118, 98, 114, 102, 92],
    [x for x in range(256) if x != 34 and x != 92],
    [92],
    [34],
    [58, 126, 63],
    [43],
    [43, 61],
    [45, 61],
    [61],
    [124],
    [124, 61],
    [38],
    [38, 61],
    [42, 37, 94, 33],
    [61],
    [61],
    [61],
    [60],
    [61],
    [60],
    [61],
    [62],
    [61],
    [62],
    [61, 62],

    [35],
    [100],
    [101],
    [102],
    [105],
    [110],
    [101],
    [105],
    [110],
    [99],
    [108],
    [117],
    [100],
    [101]
]