from setuptools import setup

with open('requirements.txt') as fp:
    install_requires = fp.read()

setup(
    name='pyclexer',
    version='0.1.0',
    py_modules=['afd', 'lexical_analyzer', 'lexical_config', 'main'],
    install_requires=install_requires,
    entry_points='''
        [console_scripts]
        clexer=main:main
    '''
)