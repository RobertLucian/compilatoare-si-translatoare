from afd import AFD

def numbered_enum(*args):
    enums = dict(zip(args, range(len(args))))
    return type('Enum', (), enums)

def ordered_dict(*args):
    d = {}
    for idx, arg in enumerate(args):
        d[idx] = arg
    return d

class LexicalAnalyzerC(AFD):

    __identifiers = [ 
        "IDENTIFIER",
        "KEYWORD",
        "LITERAL_INT",
        "LITERAL_FLOAT",
        "LITERAL_CHARACTER",
        "LITERAL_STRING",
        "OPERATOR",
        "SEPARATOR",
        "COMMENT",
        "DIRECTIVE",

        "WHITESPACE",
        "CHARACTER_ERROR",
        "CHARACTER_EOF"
        ]
    TOKENS = numbered_enum(*__identifiers) 
    DICT = ordered_dict(*__identifiers)

    KEYWORDS = [
        "abstract", "assert", "boolean", "break", "byte", "case", "catch", "char", "class", "const", "continue", "default",
        "do", "double", "else", "enum", "extends", "final", "finally", "float", "for", "goto", "if", "implements",
        "import", "instanceof", "int", "interface", "long", "native", "package", "private", "protected", "public", "return", "static",
        "short", "strictf", "super", "switch", "synchronized", "this", "throw", "transient", "try", "void", "volatile", "while"
    ]
    

    def __init__(self, no_states, final_states, transition_pairs, transition_values, input_data):
        super().__init__(no_states, final_states, transition_pairs, transition_values)
        self.input = input_data
        self.output = []
        self.__input_pos = 0

    def get_token(self):
        state = 0
        last_final_state = -1
        string = ""
        token = ""

        while state != -1:
            try:
                char = self.input[self.__input_pos]
                self.__input_pos += 1
            except IndexError:
                break
            
            # get state of given char
            state = self.get_transition(state, ord(char))
            string += char

            if self.is_final_state(state):
                token += string
                last_final_state = state
                string = ""
            # print('do something with', (ord(char), char))
        # print(token, self.__input_pos - 1)

        if state == 0:
            return (self.get_token_name_by_type(self.TOKENS.CHARACTER_EOF), self.TOKENS.CHARACTER_EOF, token, self.__input_pos)
        if last_final_state != -1:
            self.__input_pos -= len(string)
            token_type = self.get_token_type(last_final_state, token)

            if token_type == self.TOKENS.WHITESPACE or token_type == self.TOKENS.COMMENT:
                return self.get_token()
            else:
                return (self.get_token_name_by_type(token_type), token_type, token, self.__input_pos)
        return (self.get_token_name_by_type(self.TOKENS.CHARACTER_ERROR), self.TOKENS.CHARACTER_ERROR, token ,self.__input_pos)
        
    
    def get_token_type(self, state, token):
        if state == 1:
            return self.TOKENS.KEYWORD if token in self.KEYWORDS else self.TOKENS.IDENTIFIER
        elif state in [49, 56]:
            return self.TOKENS.DIRECTIVE
        elif state in [2, 4, 7, 14, 16]:
            return self.TOKENS.LITERAL_INT
        elif state in [18, 19, 20, 21, 23]:
            return self.TOKENS.LITERAL_FLOAT
        elif state == 27:
            return self.TOKENS.LITERAL_CHARACTER
        elif state == 31:
            return self.TOKENS.LITERAL_STRING
        elif state in [5, 22]:
            return self.TOKENS.SEPARATOR
        elif state == 6:
            return self.TOKENS.WHITESPACE
        elif state in [3, 8, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42]:
            return self.TOKENS.OPERATOR
        elif state in [9, 12]:
            return self.TOKENS.COMMENT
        else:
            return self.TOKENS.CHARACTER_ERROR

    def get_token_name_by_type(self, token_id):
        return self.DICT[token_id]